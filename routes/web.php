<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*
**  Login for renewing token, but did not managed to finish it so disabled it
**  URL : /login/
**  Type : POST
**  Required INPUT : email, password
**  OUTPUT : JSON structured result
*/
// $router->post('/login','LoginController@login');

/**
 * Middleware for token authenication. by doing this all connection that requires
 * privilage can be authenticated by token is correct. Current system is pretty basic
 * and not really that much secure as it made with very limited time.
 */ 
$router->group(['middleware' => 'tokenAuth'], function() use ($router) {

    /*
    **  Route to add new item with/without amount.
    **  URL : /add/
    **  Type : PUT
    **  Required INPUT : token, item_id
    **  Optional INPUT : item_amount
    **  OUTPUT : JSON structured result
    */

    $router->put('/add', 'itemController@add');

    /*
    **  Route to update item amount
    **  URL : /update/
    **  Type : POST
    **  Required INPUT : token, item_id, item_amount
    **  OUTPUT : JSON structured result
    */

    $router->post('/update', 'itemController@update');

    /*
    **  Route to remove existing item.
    **  URL : /remove/
    **  Type : POST // this is POST as we actually not delete the record.
    **  Required INPUT : token, item_id
    **  OUTPUT : JSON structured result
    */

    $router->post('/remove', 'itemController@remove');

    /*
    **  Route to view all active item list.
    **  URL : /view/
    **  Type : GET
    **  Required INPUT : token
    **  OUTPUT : JSON structured result with item list data.
    */

    $router->get('/view', 'itemController@view');

});