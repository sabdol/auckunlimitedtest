<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\RandomString as RS;
use App\Models\Item as Item;

class itemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Add item with given item name and amount, if provided
     *
     * @return string Result value as json
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'item_name' => 'required'
        ]);

        $item = new Item;

        $item->name = $request->input('item_name');
        if ($request->input('item_amount') !== null) {
            $item->amount = $request->input('item_amount');
        }
        $item->save();

        return response()->json(['status' => '0']);
    }

    /**
     * Update item amount with given item amount
     *
     * @return string Result value as json
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required',
            'item_amount' => 'required'
        ]);

        $item = Item::where('id', $request->input('item_id'))->first();

        if ($item === null) {
            return response()->json(['status' => '-1', 'message' => 'Item not exist']);
        }

        $item->active = 1;
        $item->amount = $request->input('item_amount');
        $item->update();

        return response()->json(['status' => '0']);
    }

    /**
     * Remove item with given item id.
     *
     * @return string Result value as json
     */
    public function remove(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required'
        ]);

        $item = Item::where('id', $request->input('item_id'))->first();

        if ($item === null) {
            return response()->json(['status' => '-1', 'message' => 'Item not exist']);
        }

        // I personally believe delete item in the table is bad. rather make it inactive states.
        $item->active = 0;
        $item->update();

        return response()->json(['status' => '0']);
    }

    /**
     * Return all list of active items for user.
     *
     * @return string Result value as json
     */
    public function view(Request $request)
    {

        $items = Item::where('active', 1)->get();

        return response()->json(['status' => '0', 'list' => json_encode($items)]);
    }
}
