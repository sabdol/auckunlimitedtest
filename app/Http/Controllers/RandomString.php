<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class RandomString extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getRandomStr($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkUnique($string, $table, $colume){
        $duplication = DB::table($table)->where($colume, $string)->first();
        if (count($duplication)){
            return false;
        }else{
            return true;
        }
    }

    public function getUniqueRandomStr($length, $table, $colume, $prefix = ''){
        $str = $this->getRandomStr($length);
        if($prefix !== ''){
            $str = $prefix.$str;            
        }
        $unique = $this->checkUnique($str, $table, $colume);
        if($unique){
            return $str;
        }else{
            return $this->getUniqueRandomStr($length, $table, $colume, $prefix);
        }
    }

}
