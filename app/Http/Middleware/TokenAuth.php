<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $token = $request->input('token');
            $user = User::where('active', '1')->where('api_token', $token)->first();

            if($user){

                $insert = [
                    'user_id' => $user->id,
                    'request_type' => $request->path()
                ];

                if ($request->input('item_id')) {
                    $insert['item_id'] = $request->input('item_id');
                }

                DB::table('ta_request_log')->insert($insert);

                return $next($request);
            }else{
                throw new \Exception('Invalid token.');
            }
        }
        catch(\Exception $e){

            return response()->json(['status'=>'-1', 'message'=>$e->getMessage()]);

        }
    }
}
