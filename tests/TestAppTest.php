<?php

class TestAppTest extends TestCase
{
    /**
     * /add [PUT]
     */
    public function testShouldAddItem()
    {
        $parameters = [
            'token' => '123456789',
            'item_name' => 'Test item',
        ];

        $this->PUT("add", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            ['status' => '0']    
        );
    }

    /**
     * /add [PUT]
     */
    public function testShouldAddItemWithAmount()
    {
        $parameters = [
            'token' => '123456789',
            'item_name' => 'Test item',
            'amount' => '10'
        ];

        $this->PUT("add", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            ['status' => '0']    
        );
    }

    /**
     * /add [PUT]
     */
    public function testShouldFailedAuthenticate()
    {
        $parameters = [
            'token' => '12345678',
            'item_name' => 'Test item',
            'item_amount' => '10'
        ];

        $this->PUT("add", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            ['status' => '-1', 'message' => 'Invalid token.']
        );
    }

    /**
     * /remove [POST]
     */
    public function testShouldRemoveItem()
    {
        $parameters = [
            'token' => '123456789',
            'item_id' => '1'
        ];

        $this->POST("remove", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            ['status' => '0'] 
        );
    }

    /**
     * /update [POST]
     */
    public function testShouldUpdateItem()
    {
        $parameters = [
            'token' => '123456789',
            'item_id' => '1',
            'item_amount' => '10'
        ];

        $this->POST("update", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJson(
            ['status' => '0'] 
        );
    }

    /**
     * /view [GET]
     */
    public function testShouldViewItems()
    {
        $this->GET("view?token=123456789", []);
        $this->seeStatusCode(200);
        $this->seeJson(
            [
                'status' => '0'
            ]
        );
        $this->seeJsonStructure(
            [
                'status',
                'list' => 
                [
                    '*' => 
                    [
                        'id', 
                        'name',
                        'amount',
                        'active'
                    ]
                ]
            ]
        );
    }
}