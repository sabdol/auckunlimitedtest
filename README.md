# Auckland Unlimited Test - Joon Shin

This test is base on Lumen framework.

## Containerizing

This project is containerized via docker and docker compose.

By simply run following docker command on project root will prepare image for you.

```
docker compose up -d --build
```

I really withed to finish building migration and seeding for the project but did not make it on the time and I do not want to break the project so I add sql dump file on the source.

The sql file is located on `./db_dump/testdb_dump.sql`. Please import this on your mysql images.

Please using `root` and password as `thisispassword` to gain access for mysql when you import dump file.

## Postman Documentation

Documentation for the postman usage can be found on the [postman docs](https://documenter.getpostman.com/view/16307707/TzeZDmHg).

Currently only one user is set as manully with token, did not have time to create register page and other related feature.

only user is email: test@test.com and it's token is `123456789`. I was thinking about create expiry logic and login system for reissue the token's but did not managed to do it with in the time limit.

Therefore postman only can use one token at the moment for authentication, which is simply `123456789`. So please use it for testing on postman.

## Unit testing

This project is using lumen integrated phpunit for unit testing.
on project root, execute following command to run the test

```
vendor/bin/phpunit
```

## Hour takes to doing this test

1. Setup base Lumen project - 30m
2. Setup Dockerfile, docker compose - 1h 30m
3. Build test - 45m
4. Build controller and model - 3h
5. Postman Documentation and build postman collection - 2h
6. Dump sql and Write README.md - 30m
7. Review with uni test and fixing - 30m
8. Finalize with answering other questions - 30m

## Personal thinking about the test

I am majorly focus on current company with javascript and my php and server side skill is a bit rusty and need some time to refresh my mind back to ready for test, but this was very good general practice to me to get exercise. I really enjoyed to play with it :)

## Advice on hour for best built a frontend

The system itself it pretty basic, if I managed to finished credential part, I thinks it just need, a login page, and an UI for update, add, remove all together, and another view for see the listing. Likely Another 6~8 hours or more of working, depends on design and skill of who doing this.

## What I want to do in addition if more time

Finishing token expiry, registration and login, and better authentication approach for the api, rather then passing token as a param.
